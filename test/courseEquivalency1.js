// http://rob.conery.io/2012/02/25/testing-your-model-with-mocha-mongo-and-nodejs/
// http://mongoosejs.com/docs/api.html#index_Mongoose-Model
// https://github.com/madhums/node-express-mongoose-demo/blob/master/app/models/article.js
// http://brianstoner.com/blog/testing-in-nodejs-with-mocha/

var mongoose = require("mongoose"),
  rule = require('../lib/models/rule'),
  should = require('should'),
  Promise = require('bluebird'),
  CourseEquivalency = require('../lib/courseEquivalency').CourseEquivalency,
  options = { db: 'mongodb://localhost/transfer'},

  sampleDataEquivData = [
    {
      "SCHOOL_CRSE_NBR": "101",
      "SCHOOL_SUBJECT": "ENGL",
      "TRNSFR_SRC_ID": "1332634",
      "CLASS_TAKEN": "FALL 2006"
    },

    {
      "SCHOOL_CRSE_NBR": "125",
      "SCHOOL_SUBJECT": "MATH",
      "TRNSFR_SRC_ID": "1332634",
      "CLASS_TAKEN": "FALL 2007"
    },

    {
      "SCHOOL_CRSE_NBR": "102",
      "SCHOOL_SUBJECT": "ENGL",
      "TRNSFR_SRC_ID": "1332634",
      "CLASS_TAKEN": "FALL 2007"
    },

    {
      "SCHOOL_CRSE_NBR": "127",
      "SCHOOL_SUBJECT": "CSC",
      "TRNSFR_SRC_ID": "1332634",
      "CLASS_TAKEN": "FALL 2001"
    }

  ];



describe('Checking value', function(){

  // Test class translator
  it('using translator', function(done){
    var ce  = new CourseEquivalency(options, sampleDataEquivData);
    ce.getEquivalencyPromises()
      .then(function(data){
        console.log(data);
        done();
      });
  });

});