// http://rob.conery.io/2012/02/25/testing-your-model-with-mocha-mongo-and-nodejs/
// http://mongoosejs.com/docs/api.html#index_Mongoose-Model
// https://github.com/madhums/node-express-mongoose-demo/blob/master/app/models/article.js
// http://brianstoner.com/blog/testing-in-nodejs-with-mocha/

var mongoose = require("mongoose"),
  rule = require('../lib/models/rule'),
  should = require('should'),
  Promise = require('bluebird'),
  options = { db: 'mongodb://localhost/transfer'},

  sampleData =    {
      SCHOOL_CRSE_NBR: "225",
      SCHOOL_SUBJECT: "MATH",
      END_DT: { $gte: new Date(1314860400000) },
      BEGIN_DT: { $lte: new Date(1314860400000) },
      TRNSFR_SRC_ID: "1332634"
    }


var mongPromise = function(){
  return new Promise(function (resolve, reject){
    rule.Model.getCourse(sampleData, function(err, results){
      if (err){
        reject(err);
      }else{
        resolve(results);
      }
    });
  })
}



dbURI = 'mongodb://localhost/transfer';

describe('Checking value', function(){



  beforeEach(function(done) {
    if (mongoose.connection.db) return done();
    mongoose.connect(dbURI, done);
  });

  /*
  it("retrieves by external org id", function(done){
    //console.log(rule.Model.getCourse());
    rule.Model.getCourse(sampleData, function(err, results){
      results.forEach(function(value){
        value.SUBJECT.should.equal('MATddd');
        value.CATALOG_NBR.trim().should.equal('200TR');
      });
    });
  });
  */

  /*
  it('using mongo promise', function(done){
    mongPromise()
      .then(function(results){
        //console.log(results);
        return results;
      })
      .then(function (results){
        results.forEach(function(value){
          value.SUBJECT.should.equal('MAT');
          value.CATALOG_NBR.trim().should.equal('200TR');
        });
        return results;
      })
      .then(function(results){
        results.forEach(function(value){
          console.log(value.SUBJECT);
          console.log(value.CATALOG_NBR.trim());
        })
        return results;
      }).then(function(results){
        done()
      })

      .error(function(e){console.log("Error handler " + e)})
      .catch(function(e){console.log("Catch handler " + e)});
  });
*/

});