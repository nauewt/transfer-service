// http://rob.conery.io/2012/02/25/testing-your-model-with-mocha-mongo-and-nodejs/
// http://mongoosejs.com/docs/api.html#index_Mongoose-Model
// https://github.com/madhums/node-express-mongoose-demo/blob/master/app/models/article.js
// http://brianstoner.com/blog/testing-in-nodejs-with-mocha/

var mongoose = require("mongoose"),
  rule = require('../lib/models/rule'),
  should = require('should'),
  Promise = require('bluebird'),
  CourseEquivalency = require('../lib/courseEquivalency').CourseEquivalency,
  options = { db: 'mongodb://localhost/transfer'},

  sampleDataEquivData = [
    {
      "SCHOOL_CRSE_NBR": "101",
      "SCHOOL_SUBJECT": "ENG",
      "TRNSFR_SRC_ID": "1332627",
      "CLASS_TAKEN": "FALL 2010"
    },
    {
      "SCHOOL_CRSE_NBR": "102",
      "SCHOOL_SUBJECT": "ENG",
      "TRNSFR_SRC_ID": "1332627",
      "CLASS_TAKEN": "SPRING 2011"
    }

  ];


sampleDataEquivData2 = [
  {
    "SCHOOL_CRSE_NBR": "101",
    "SCHOOL_SUBJECT": "ENG",
    "TRNSFR_SRC_ID": "1332627",
    "CLASS_TAKEN": "FALL 2005"
  }

];


sampleDataEquivData3 = [

  {
    "SCHOOL_CRSE_NBR": "101",
    "SCHOOL_SUBJECT": "ENG",
    "TRNSFR_SRC_ID": "1332627",
    "CLASS_TAKEN": "FALL 2010"
  },
  {
    "SCHOOL_CRSE_NBR": "102",
    "SCHOOL_SUBJECT": "ENG",
    "TRNSFR_SRC_ID": "1332627",
    "CLASS_TAKEN": "SPRING 2001"
  }

];

describe('Start Test', function(){
  var ce  = new CourseEquivalency(options, sampleDataEquivData);
  // Test class translator
  it('Courses taken together', function(done){

    ce.getEquivalencyPromises()
      .then(function(data){
        //console.log(data[0].RULES)
        //data[0].EQUIVALENT[0].should.equal('CJ100TR');
        console.log(data);
        done();
      });
  });



  it('One one Course Taken', function(done){
    var ce2  = new CourseEquivalency(options, sampleDataEquivData2);
    ce2.getEquivalencyPromises()
      .then(function(data){
        //console.log(data)
        //data[0].EQUIVALENT[0].should.equal('CJ100TR');
        console.log(data);
        done();
      });
  });

  it('One one Course Taken', function(done){
    var ce3  = new CourseEquivalency(options, sampleDataEquivData3);
    ce3.getEquivalencyPromises()
      .then(function(data){
        //console.log(data)
        //data[0].EQUIVALENT[0].should.equal('CJ100TR');
        console.log(data)
        done();
      });
  });

});