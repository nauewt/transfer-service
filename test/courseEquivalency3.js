// http://rob.conery.io/2012/02/25/testing-your-model-with-mocha-mongo-and-nodejs/
// http://mongoosejs.com/docs/api.html#index_Mongoose-Model
// https://github.com/madhums/node-express-mongoose-demo/blob/master/app/models/article.js
// http://brianstoner.com/blog/testing-in-nodejs-with-mocha/

var mongoose = require("mongoose"),
  rule = require('../lib/models/rule'),
  should = require('should'),
  Promise = require('bluebird'),
  CourseEquivalency = require('../lib/courseEquivalency').CourseEquivalency,
  options = { db: 'mongodb://localhost/transfer'},

  sampleDataEquivData = [
    {
      "SCHOOL_CRSE_NBR": "001A",
      "SCHOOL_SUBJECT": "CHEM",
      "TRNSFR_SRC_ID": "1332897",
      "CLASS_TAKEN": "FALL 2006"
    }


  ];



describe('Checking value', function(){

  // Test class translator
  it('using translator', function(done){
    var ce  = new CourseEquivalency(options, sampleDataEquivData);
    ce.getEquivalencyPromises()
      .then(function(data){
        data[0].EQUIVALENT[0].should.equal('CHM151');
        data[0].EQUIVALENT[1].should.equal('CHM151L');
        console.log(data);
        done();
      });
  });

});