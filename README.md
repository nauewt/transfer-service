# Jacks' Transfer Services

Provides a simplified API for accessing the actual data services that feed Jacks' Transfer

## Main include

When you require('transfer-service'), you will get an object with the following properties (see below for descriptions of classes).

* CourseEquivalency
* Catalog
* WhatIf

## CourseEquivalency

The course to course equivalency engine class provides equivalencies for courses from other institutions to NAU courses.

### Constructor

'''new CourseEquivalency(options)'''

Options:

* derp: herp

### getEquivalency(Array<Object>)

The format of individual courses in the input is:

* derp: herp

Returns a promise that when resolved supplies an Array<Object> of NAU course equivalencies with some meta data.


## Catalog

Provides methods for accessing and working with NAU course catalog data.

### Catalog

'''new Catalog(options)'''

Options:

* derp: herp


## WhatIf

Provides an interface for running a set of NAU courses against a particular plan in order to gauge completion in detail.

### WhatIf

'''new WhatIf(options)'''

Options:

* derp: herp

* * *
## PeopleSoft Web Services

###runDegreeProgress
Runs a degree progress report against the provided plan utilizing the provided courses

analysis_db_seq can be anything(4 digit number) and emplid(11 characters) as well.  Maybe start emplid with TRF or something.  

```
<nau_ows>
   <uid></uid>
   <version>1</version>
   <operation>runDegreeProgress</operation>
   <emplid>AAAAA</emplid>
   <analysis_db_seq>9</analysis_db_seq>
   <acad_plan>ACBSACCYX</acad_plan>
   <saa_rpt_crswhif>
		<crse_id>000005</crse_id>
		<crse_id>000006</crse_id>
   </saa_rpt_crswhif>
</nau_ows>
```