'use strict';
var moment = require('moment'),
  _ = require("lodash"),
  mongoose = require('mongoose'),
  models = require('./models'),
  Plancourse = models.TransPlancourse,
  AcadPlan = models.TransAcadplan,
  Promise = require('bluebird');



var CatalogData = function( options ){
  this.db = options.db || 'test';

  if (  mongoose.connection.readyState === 0 ) {
    mongoose.connect(this.db);
  }
}

/**
 * Return data from all plans
 * @returns {Promise}
 */
var getPlanData = function() {
  return new Promise(function (resolve, reject){
    Plancourse.getPlanData(function(err, results){
      if (err){
        console.log(err);
        reject(err);
      }else{
        resolve(results);
      }
    });
  })
}


var preProcessPlanData = function (data){
  return new Promise(function (resolve, reject) {
    if (data.length == 0){
      reject('Empty Data Set');
    }

    var mappedData = data.map(function(dat){
      var acadPlan = dat.ACAD_PLAN,
        crseId = _.uniq(dat.CRSE_ID.split('|')),
        acadPlanDescr = dat.ACAD_PLAN_DESCR;
      dat = dat.toObject();

      dat.acadPlan = acadPlan;
      dat.crseId = crseId;
      dat.acadPlanDescr = acadPlanDescr;
      return dat;
    })
    resolve(mappedData);
  });
}


CatalogData.prototype.getPlanData = function(){
  return getPlanData()
    .then(preProcessPlanData.bind(this))
};

CatalogData.prototype.showTopPlans = function(studentInfo){
  var finalTally = [];
  return this.getPlanData().
    then(function(planData){
      planData.forEach(function(pData){
        var crseMatchArr = _.intersection(pData.crseId, studentInfo),
          crseMatchCnt = crseMatchArr.length;
        if( crseMatchCnt ) {
          finalTally.push({acadPlan: pData.acadPlan, matchCount: crseMatchCnt, planDescr: pData.acadPlanDescr});
        }
      })
      // Sort and return the top 5
      return _.takeRight(_.sortBy(finalTally, 'matchCount'), 5);
    })

}

CatalogData.prototype.getCategory = function(query){
  return new Promise(function (resolve, reject){

    // If query is provided then display plans which fall under that category
    if (query && Object.keys(query).length){
      AcadPlan.getCategory(query, function(err, results){
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    }else {
      AcadPlan.getCategory(null, function (err, results) {
        if (err) {
          reject(err);
        } else {
          var catObject = {},
            finalCategory = [];

          results.forEach(function (data) {
            // Data is in this format |AAA|CCC|
            // So we remove the first and last element because they are always empty
            var arr = data.N__CATEGORY_LIST.split('|');
            arr.shift();
            arr.pop();
            arr.forEach(function (arrDat) {
              catObject[arrDat] = true;
            });
          })

          Object.keys(catObject).forEach(function (data) {
            finalCategory.push(data);
          })
          resolve(finalCategory.sort());
        }
      })
    }
  })
}

CatalogData.prototype.getCampus = function(query){
  return new Promise(function (resolve, reject){

    // If query is provided then display plans which fall under that category
    if (query  && Object.keys(query).length){
      AcadPlan.getCampus(query, function(err, results){
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    }else {
      AcadPlan.getCampus(null, function (err, results) {
        if (err) {
          reject(err);
        } else {
          var catObject = {},
            finalCategory = [];

          results.forEach(function (data) {
            // Data is in this format |AAA|CCC|
            // So we remove the first and last element because they are always empty
            var arr = data.N__CAMPUS_LIST.split('|');
            arr.shift();
            arr.pop();
            arr.forEach(function (arrDat) {
              catObject[arrDat] = true;
            });
          })

          Object.keys(catObject).forEach(function (data) {
            finalCategory.push(data);
          })
          resolve(finalCategory.sort());
        }
      })
    }
  })
}


CatalogData.prototype.getPlans = function(query){

  return new Promise(function (resolve, reject) {
    if (query) {
      AcadPlan.getPlans(query, function (err, results) {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      })
    }
  });

}

module.exports.CatalogData = CatalogData;