'use strict';
var  _ = require("lodash"),
  mongoose = require('mongoose'),
  models = require('./models'),
  Org = models.TransOrganization,
  Promise = require('bluebird');

var Organization = function (options){
  this.db = options.db || 'test';
  if (  mongoose.connection.readyState === 0 ) {
    mongoose.connect(this.db);
  }
}

Organization.prototype.getOrg = function(query){
  return new Promise(function (resolve, reject){
    Org.getOrganizationData(query, function(err, results){
      if (err){
        console.log(err);
        reject(err);
      }else{
        resolve(results);
      }
    });
  });
}


module.exports.Organization = Organization;