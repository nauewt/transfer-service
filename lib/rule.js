
models = require('./models'),
  rule = models.TransRule,
  mongoose = require('mongoose'),
  Promise = require('bluebird');


var Rule = function (options){
  this.db = options.db || 'test';
  if (  mongoose.connection.readyState === 0 ) {
    mongoose.connect(this.db);
  }
}


Rule.prototype.getCourse = function(query){
  return new Promise(function (resolve, reject){
    rule.getCourse(query, function(err, results){
      if (err){
        console.log(err);
        reject(err);
      }else{
        resolve(results);
      }
    });
  });
}


module.exports.Rule = Rule;