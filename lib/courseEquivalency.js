'use strict';
var Promise = require('bluebird'),
  mongoose = require('mongoose'),
  moment = require('moment'),
  async = require('async'),
  models = require('./models'),
  _ = require('lodash'),
  Rule = models.TransRule;


var CourseEquivalency = function (options, cartData) {
  this.db = options.db || 'test';
  this.cartData = cartData || [];
  this.allCoursesList = {};
  if (  mongoose.connection.readyState === 0 ) {
    mongoose.connect(this.db);
  }
};


/**
 * Presprocess course data
 * @param data
 */
var preProcessData = function (cartData){

  //console.log(this)

  return new Promise(function (resolve, reject) {
    if (cartData.length == 0){
      reject('Empty Data Set');
    }

    cartData.map(function (course) {

      var orgId = course.TRNSFR_SRC_ID.toString(),
        coursePre = course.SCHOOL_SUBJECT,
        courseNbr = course.SCHOOL_CRSE_NBR.toString(),
        courseName = coursePre + courseNbr,
        institution = course.INSTITUTION;

      // BEGIN: Edge case for Maricopa community colleges
      if (
        orgId === '1332635' || orgId === '1335330' || orgId === '1332638' ||
        orgId === '1332630' || orgId === '1332632' || orgId === '1332696' ||
        orgId === '1332633' || orgId === '1332641' || orgId === '1332643'
      ) {
        orgId = '1367986';
      }
      // END: Edge case for Maricopa community colleges

      // Done preprocesses, reassemble
      course.orgId = orgId;
      course.courseName = courseName;
      course.uniqueId = orgId + '_' + courseName;
      course.institution = institution;


      return course;
    });
    resolve(cartData);
  }).bind(this);
};


/**
 * Calculate catalog year from the year selected
 * @param classTaken
 * @returns {string}
 */

var catalogYear = function(classTaken){
  //console.log(classTaken)
  var classTakenArr = classTaken.split(' '),
    year = classTakenArr[1],
    catYear = year.substring(year.length - 2),
    nYear;

  if (classTakenArr[0].toLowerCase() === 'fall' ){
    nYear = parseInt(catYear) + 1;
    if (nYear < 10){
      catYear = catYear + '0' + nYear;
    }else {
      catYear = catYear + nYear;
    }
    return catYear;
  }else{
    nYear = parseInt(catYear) - 1;
    if (nYear < 10){
      catYear = '0' + nYear + catYear;
    }else {
      catYear = nYear + catYear;
    }
    return catYear;
  }
};

/**
 * Calculated date based on the semester selected
 * @param classTaken
 * @returns {*}
 */
var classTakenDate = function(classTaken){

  var classTakenArr = classTaken.split(' ');
  var classTakenFinalDate;
  if (classTakenArr[0] === 'FALL' || classTakenArr[0] === 'Fall' ) {
    classTakenFinalDate = moment(classTakenArr[1] + '09' + '1', 'YYYY-MM-DD');
  }else if(classTakenArr[0] === 'SPRING' || classTakenArr[0] === 'Spring'){
    classTakenFinalDate = moment(classTakenArr[1] + '02' + '1', 'YYYY-MM-DD');
  }else{
    classTakenFinalDate = moment(classTakenArr[1] + '06' + '1', 'YYYY-MM-DD');
  }

  return classTakenFinalDate.format();

};

var getDateRange = function(dateStr){
  var dateIn = dateStr;
  var regexp = /([a-z]+)([0-9]+)-([a-z]+)(\d+)/i;
  var arrDate = dateIn.match(regexp);
  var lowerBound;
  var upperBound;
  var semesterMap = {
    'F'  : 9,
    'SP' : 2,
    'S'  : 6
  };
  var dates = [];
  var lowerYear;
  var upperYear;
  var endDate;
  var endMonth;

// Calculate year
  if ( arrDate[2] > 50 ){
    lowerYear = '19'+arrDate[2];
  }else {
    lowerYear = '20'+arrDate[2];
  }

  if ( arrDate[4] > 50 ){
    upperYear = '19'+arrDate[4];
  } else{
    upperYear = '20'+arrDate[4];
  }

// Calculate end dates

  if ( arrDate[3] === 'F'  ){
    endDate = '31';
    endMonth = '01';
    upperYear = parseInt(upperYear) + 1;
  }else if( arrDate[3] === 'SP'){
    endDate = '31';
    endMonth = '05';
  }else{
    endDate = '31';
    endMonth = '08';
  }

  lowerBound = moment(lowerYear + '-' + semesterMap[arrDate[1]] + '-1', 'YYYY-MM-DD');
  upperBound = moment(upperYear + '-' + endMonth + '-' + endDate, 'YYYY-MM-DD');

  dates.push(lowerBound.format());
  dates.push(upperBound.format());
  return dates;
};


/**
 * Check to see if the course was taken data ranges passed
 * @param dateA
 * @param dateB
 * @param classTaken
 * @returns {boolean}
 *
*/

var calculateOverlap =  function(dateA, classTaken){
  // does classTaken fall in between these dates
  //console.log(dateA)
  //console.log(dateB)
  var date1 = getDateRange(dateA);
  //var date2 = getDateRange(dateB);

  //console.log(date1);
  //console.log(date2);
  if (
    moment(classTaken).diff(moment(date1[0])) >= 0 && moment(classTaken).diff(moment(date1[1])) <= 0
  ){
    return true;
  }else{
    return false;
  }

};






var buildRelatedClassArr = function (coursesArr, cartData, relatedClassArray){

  var arrDiff = 1;

  /**
   * This step is done so that we do not include a related class twice
   */
  relatedClassArray.forEach(function (val){
    var diff = _.difference(val, coursesArr);
    if (diff.length === 0){
      arrDiff = 0;
    }
  })


  if ( coursesArr.length > 1 && arrDiff !== 0){
    // Do not insert the current class in the related class array
    relatedClassArray.push(_.without(coursesArr, cartData.transCrseNbr))
  }



  return relatedClassArray;
}



/**
 * Build related courses and course rules
 * @param queryResults
 * @param cartData
 * @returns {{}}
 */
var buildCourseData = function(queryResults, cartData){

  var timeFrame = '',
    ruleSet = {},
    equivCrse = {},
    extUnitSet = {},
    unitSet = {},
    rulesArr = [],
    lastRule,
    timeFrames = {},
    crseIdArr = [],
    allClasses = [],
    relatedClass =[],
    relatedClassArray = [],
    ruleTimeFrameArr = [],
    ruleTimeFrame = {},
    extUnitsArr = [],
    unitsArr = [];

  queryResults.forEach(function (values) {
    var crseRule = (typeof values.DESCR100 === 'undefined' ) ? 'No Equivalent' : values.DESCR100.split(':'),
      coursesArr = crseRule[0].split('\/');
    // Calculating time frame from DESCR100 to add to courseData "DESCR100" : "221/222/223/227/228/229: F99-F15",

    timeFrame = crseRule[1].trim();
    timeFrames[timeFrame] = getDateRange(crseRule[1]);
    //timeFrames[timeFrame] = crseRule[0];

    //relatedClassArray.push(coursesArr);

    //allClasses = buildAllClassesArray(coursesArr, allClasses);
    //relatedClass = buildRelatedClassArr(coursesArr, cartData, relatedClass);
    relatedClassArray = buildRelatedClassArr(coursesArr, cartData, relatedClassArray);

    if (lastRule === undefined) {
      lastRule = values.SCHOOL_SUBJECT + coursesArr; //lastRule = BI211,212,213
      rulesArr.push(values.SUBJECT.trim() + values.CATALOG_NBR.trim());
      crseIdArr.push(values.CRSE_ID);
      extUnitsArr.push(values.EXT_UNITS);
      unitsArr.push(values.UNT_TAKEN);
      if ( ruleTimeFrameArr.indexOf(timeFrame) === -1) {
        ruleTimeFrameArr.push(timeFrame);
      }
      //ruleSet[coursesArr] = rulesArr
      ruleSet[coursesArr] = rulesArr;
      equivCrse[coursesArr] = crseIdArr;
      ruleTimeFrame[coursesArr] = ruleTimeFrameArr;
      extUnitSet[coursesArr] = extUnitsArr;
      unitSet[coursesArr] = unitsArr;
      //console.log("First time setting rule")
      // RuleSet 211,212,213 this is the key
      // rulesArr = [ 'BIO181' ] is the value
    } else {
      //console.log("lastRule variable set")
      // We have a new rule jump in this if
      // A new Subject so we blank the array
      if (lastRule !== values.SCHOOL_SUBJECT + coursesArr) {
        // Blank out the rulesArr. We have a new rule
        rulesArr = [];
        crseIdArr = [];
        ruleTimeFrameArr =[];
        extUnitsArr = [];
        unitsArr = [];
        //"SUBJECT" : "BIO", "CATALOG_NBR" : " 182L"
        rulesArr.push(values.SUBJECT.trim() + values.CATALOG_NBR.trim());
        crseIdArr.push(values.CRSE_ID);
        extUnitsArr.push(values.EXT_UNITS);
        unitsArr.push(values.UNT_TAKEN);
        if ( ruleTimeFrameArr.indexOf(timeFrame) === -1) {
          ruleTimeFrameArr.push(timeFrame);
        }
        //ruleSet[values.SCHOOL_SUBJECT + coursesArr] = rulesArr
        ruleSet[coursesArr] = rulesArr;
        equivCrse[coursesArr] = crseIdArr;
        ruleTimeFrame[coursesArr] = ruleTimeFrameArr;
        extUnitSet[coursesArr] = extUnitsArr;
        unitSet[coursesArr] = unitsArr;
        lastRule = values.SCHOOL_SUBJECT + coursesArr;

      } else {
        // This class combo has more than one rule so we jump into this statement
        // Example 'ENGL 101,102': 'ENG 105
        // Example 'ENGL 101,102': ENG 100TR'

        rulesArr.push(values.SUBJECT.trim() + values.CATALOG_NBR.trim());
        crseIdArr.push(values.CRSE_ID);
        extUnitsArr.push(values.EXT_UNITS);
        unitsArr.push(values.UNT_TAKEN);
        if ( ruleTimeFrameArr.indexOf(timeFrame) === -1) {
          ruleTimeFrameArr.push(timeFrame);
        }
        //ruleSet[values.SCHOOL_SUBJECT + coursesArr] =  rulesArr
        ruleSet[coursesArr] = rulesArr;
        equivCrse[coursesArr] = crseIdArr;
        ruleTimeFrame[coursesArr] = ruleTimeFrameArr;
        extUnitSet[coursesArr] = extUnitsArr;
        unitSet[coursesArr] = unitsArr;
        lastRule = values.SCHOOL_SUBJECT + coursesArr;

      }
    }
  });

  //console.log(timeFrames)

  return {
    course: cartData.transClsName,
    relatedCourse: relatedClass,
    rules: ruleSet,
    timeFrame: timeFrame,
    calculatedDate: cartData.classTakenFinalDate,
    crseId: equivCrse,
    catYear: cartData.catYear,
    relatedClassArr: relatedClassArray,
    timeFrames: ruleTimeFrame,
    extUnits: extUnitSet,
    units: unitSet
  };

}

var isEcorPl = function(campus){
  campus = campus.COMP_SUBJECT_AREA.split(" ")[0];
  var regex = new RegExp("(EC|PL)$")
  if ( regex.test(campus) ){
    return false;
  }else {
    return true;
  }
}

/**
 * Get course detail from mongo
 * @param options
 * @returns {Promise}
 */

var getCourseData = function(options) {
  return new Promise(function (resolve, reject){
    Rule.getCourseDetail(options, function(err, results){
      if (err){
        console.log(err);
        reject(err);
      }else{
        var modResults = results.filter(isEcorPl);
        resolve(modResults);
      }
    });
  })
}


var calculateEquivalencies = function(uniqueId, relatedClasses, currentProcessedCourseData, finalObject, allRelatedTaken, transClsName, transCrseNbr, classKeys, allRelatedClsNbrArr) {

  //console.log(currentProcessedCourseData);

  // This action is taken so to add the original class back.  This array contains all the related class numbers
  // and does not include the class itself. We the current class to it and sort it.
  // This comes in handy because the rules object key has these values.  We use this to pull the right rule.
  if (allRelatedClsNbrArr.length > 0) {
    allRelatedClsNbrArr.push(finalObject.SCHOOL_CRSE_NBR);
    allRelatedClsNbrArr.sort();
  }


  if (relatedClasses.length < 1) {
    // This block means that the class does not have a related class or the
    // class was taken without the related class.  ENG 101 was taken by itself without 102

    var equivalent = currentProcessedCourseData.rules[transCrseNbr] ? currentProcessedCourseData.rules[transCrseNbr] : ['No Equivalent'];
    var crseEquivalent = currentProcessedCourseData.crseId[transCrseNbr] ? currentProcessedCourseData.crseId[transCrseNbr] : ['No Equivalent'];

    if (equivalent.indexOf('NONENONE') > -1 || equivalent.indexOf('NONE') > -1) {
      equivalent = ['Not Transferable'];
    }

    finalObject.RELATEDCLASS = currentProcessedCourseData.relatedClassArr;
    finalObject.EQUIVALENT = equivalent;
    finalObject.RULES = currentProcessedCourseData.rules;
    finalObject.COMBINED = false;
    finalObject.CRSE_ID = crseEquivalent;
    finalObject.EXT_UNITS = currentProcessedCourseData.extUnits[transCrseNbr] ? currentProcessedCourseData.extUnits[transCrseNbr] : 0;
    finalObject.UNITS = currentProcessedCourseData.units[transCrseNbr] ? currentProcessedCourseData.units[transCrseNbr] : 0;

  }else{

    if (allRelatedTaken) {

      // allRelatedTaken means that we check to see if all the related classes are taken
      // This does not mean we took them in the same time frame.

      var currClassTimeFrames = [],
        relateClassTimeFrames = [],
        timeFrames = [];

      // For the currentClass iterate over timeFrames and
      // Throw these timeFrames into currClassTimeFrames array
      // timeFrames: { '102': [ 'F99-F15' ], '101,102': [ 'F04-F15' ] } }
      Object.keys(currentProcessedCourseData.timeFrames).forEach(function(tFrames){
        //console.log(currentProcessedCourseData.timeFrames[tFrames]);
        currClassTimeFrames.push(currentProcessedCourseData.timeFrames[tFrames].join(''));
      });

      // Check to see if the class Taken date falls in the timeFrames provided
      currClassTimeFrames.forEach(function(dates){
        // Check to see if the class Taken date falls in the timeFrame
        if ( calculateOverlap(dates, currentProcessedCourseData.calculatedDate)) {
          timeFrames.push(dates);
        }
      });

      // For the currentClass's related classes iterate over timeFrames and
      // Throw these timeFrames into relatedClassTimeFrames array
      // timeFrames: { '102': [ 'F99-F15' ], '101,102': [ 'F04-F15' ] } }
      relatedClasses.forEach(function(clas){
        //console.log(classKeys[clas].timeFrames);
        // Pull the timeFrames key for each related class
        Object.keys(classKeys[clas].timeFrames).forEach(function(dates){
          // Check to see if the class Taken date falls in the timeFrames provided
          if ( calculateOverlap(classKeys[clas].timeFrames[dates].join(''), classKeys[clas].calculatedDate)) {
            relateClassTimeFrames.push(classKeys[clas].timeFrames[dates].join(''));
          }
        });
      })

      // We intersect the current class timesFrames to the relatedClasses' timeFrames
      // This will give all valid times for true for the current and all its related classes
      var intersectedTimeFrame = _.intersection(timeFrames, relateClassTimeFrames);

      //
      if ( _.intersection(currentProcessedCourseData.timeFrames[allRelatedClsNbrArr] , intersectedTimeFrame).length >= 1  ){
        // Classes taken in the same time give combined credit
        // The intersection proves that classes were both taken in a given timeFrame
        console.log('Class taken in the same time frame');

        var clsRule = allRelatedClsNbrArr.join(',');
        equivalent = currentProcessedCourseData.rules[clsRule] ? currentProcessedCourseData.rules[clsRule] : ['No Equivalent'];
        crseEquivalent = currentProcessedCourseData.crseId[clsRule] ? currentProcessedCourseData.crseId[clsRule] : ['No Equivalent'];

        // Display data consistently for courses taken in the correct time frame
        if (equivalent.indexOf('NONENONE') > -1 || equivalent.indexOf('NONE') > -1) {
          equivalent = ['Not Transferable'];
        }

        finalObject.RELATEDCLASS = currentProcessedCourseData.relatedClassArr;
        finalObject.EQUIVALENT = equivalent;
        finalObject.RULES = currentProcessedCourseData.rules;
        finalObject.COMBINED = true;
        finalObject.CRSE_ID = crseEquivalent;
        finalObject.EXT_UNITS = currentProcessedCourseData.extUnits[clsRule] ? currentProcessedCourseData.extUnits[clsRule] : 0;
        finalObject.UNITS = currentProcessedCourseData.units[clsRule] ? currentProcessedCourseData.units[clsRule] : 0;

      }else{
        console.log('Related class not taken in the same time frame ');
        equivalent = currentProcessedCourseData.rules[transCrseNbr] ? currentProcessedCourseData.rules[transCrseNbr] : ['No Equivalent'];

        crseEquivalent = currentProcessedCourseData.crseId[transCrseNbr] ? currentProcessedCourseData.crseId[transCrseNbr] : ['No Equivalent'];
        if (equivalent.indexOf('NONENONE') > -1 || equivalent.indexOf('NONE') > -1) {
          equivalent = ['Not Transferable'];
        }

        finalObject.RELATEDCLASS = currentProcessedCourseData.relatedClassArr;
        finalObject.EQUIVALENT = equivalent;
        finalObject.RULES = currentProcessedCourseData.rules;
        finalObject.COMBINED = false;
        finalObject.CRSE_ID = crseEquivalent;
        finalObject.EXT_UNITS = currentProcessedCourseData.extUnits[transCrseNbr] ? currentProcessedCourseData.extUnits[transCrseNbr] : 0;
        finalObject.UNITS = currentProcessedCourseData.units[transCrseNbr] ? currentProcessedCourseData.units[transCrseNbr] : 0;

      }

    } else {
      // Related classes not taken
      //console.log('All related classes not taken')
      equivalent = currentProcessedCourseData.rules[transCrseNbr] ? currentProcessedCourseData.rules[transCrseNbr] : ['No Equivalent'];
      crseEquivalent = currentProcessedCourseData.crseId[transCrseNbr] ? currentProcessedCourseData.crseId[transCrseNbr] : ['No Equivalent'];
      //console.log(equivalent);

      if (equivalent.indexOf('NONENONE') > -1 || equivalent.indexOf('NONE') > -1) {
        equivalent = ['Not Transferable'];
      }

      finalObject.RELATEDCLASS = currentProcessedCourseData.relatedClassArr;
      finalObject.EQUIVALENT = equivalent;
      finalObject.RULES = currentProcessedCourseData.rules;
      finalObject.COMBINED = false;
      finalObject.CRSE_ID = crseEquivalent;
      finalObject.EXT_UNITS = currentProcessedCourseData.extUnits[transCrseNbr] ? currentProcessedCourseData.extUnits[transCrseNbr] : 0;
      finalObject.UNITS = currentProcessedCourseData.units[transCrseNbr] ? currentProcessedCourseData.units[transCrseNbr] : 0;
    }
  }

  //console.log(finalObject);
  return finalObject;
}

var processRuleData = function (cartData) {
  return Promise.map(cartData, (function (cartDatum) {
      var courseData = {},
        cartDataObj = {
          orgId : cartDatum.orgId,
          origOrgId : cartDatum.TRNSFR_SRC_ID.toString(),
          transPrefix : cartDatum.SCHOOL_SUBJECT ,
          transCrseNbr : cartDatum.SCHOOL_CRSE_NBR.toString() ,
          transClsName : cartDatum.courseName,
          uniqueId : cartDatum.uniqueId,
          classTaken : cartDatum.CLASS_TAKEN,
          classTakenFinalDate : classTakenDate(cartDatum.CLASS_TAKEN),
          catYear : catalogYear(cartDatum.CLASS_TAKEN)
        };

      return getCourseData(cartDataObj)
        .then(function(queryResults){
          var queryFormattedData = buildCourseData(queryResults, cartDataObj)
          courseData[cartDataObj.uniqueId] = queryFormattedData;
          return courseData;
        })
    })
  )
};


var processEquivalencies = function (processedCartData) {
  //console.log(processedCartData);

  var classKeys = {};


  // This is a convenience step.  ProcessesedCart Data is an
  // array [ { crseid : { CourseInfo}, ... ]
  // Converted it into a object like so
  // { { crseid : { CourseInfo}, ... }
  processedCartData.forEach(function (classKey){
    Object.keys(classKey).forEach(function(val){
      classKeys[_.keys(classKey)] = classKey[val];
    })
  })

  //console.log(classKeys)

  return Promise.map(this.cartData, (function (course) {
      var transPrefix = course.SCHOOL_SUBJECT,
        transCrseNbr = course.SCHOOL_CRSE_NBR.toString(),
        transClsName = transPrefix + transCrseNbr,
        uniqueId = course.uniqueId,
        finalObject = {};

      // Populate from original cart data
      finalObject.TRNSFR_SRC_ID = course.orgId;
      finalObject.SCHOOL_CRSE_NBR = course.SCHOOL_CRSE_NBR;
      finalObject.SCHOOL_SUBJECT = course.SCHOOL_SUBJECT;
      finalObject.CLASS_TAKEN = course.CLASS_TAKEN;
      finalObject.INSTITUTION = course.institution;
      //finalObject.CAT_YEAR = course[transClsName].catYear;

      // This is done for Maricopa School system
      if (course.orgId === '1367986') {
        finalObject.TRNSFR_SRC_ID = course.TRNSFR_SRC_ID.toString();
      }

      console.log('')
      console.log('Calculating final equivalencies ' + finalObject.SCHOOL_SUBJECT + ' ' + finalObject.SCHOOL_CRSE_NBR + ' ' + finalObject.CLASS_TAKEN);
      var currentProcessedCourse = classKeys[uniqueId];

      //console.log(currentProcessedCourse)
      finalObject.CAT_YEAR = currentProcessedCourse.catYear;
      finalObject.TIME_FRAME = currentProcessedCourse.timeFrames;

      //console.log(currentProcessedCourse.relatedClassArr);

      var relatedClassArr = currentProcessedCourse.relatedClassArr,
        allRelatedTaken = false,
        relatedClasses = [],
        allRelatedClsNbr = [];

      console.log(relatedClassArr);
      console.log('Class array length ' + relatedClassArr.length);

      // This function checks to see all related classes are taken or not
      var allClassesTaken = function (element) {
        return classKeys.hasOwnProperty(element) === true;
      }


      if ( relatedClassArr.length > 1 ){
        // This means we have an OR combo  ("001A" and "001B") or ("001A" and "001B") EG. [ ['101B'], ['101C'] ]
        // This is an edge case and is handled differently

        //console.log('Related class array size ' + relatedClassArr.length)
        //console.log(relatedClassArr);

        /**
         * Populate groupRelatedClasses with true of false values
         * if all related in the group are taken
         * The array looks like [ ['001A', '001B'], [ '001A', '001B'] ]
         * are all related classes taken
         */
        var groupRelatedClasses = []
        relatedClassArr.forEach(function(classArr, index){
          classArr.forEach(function (val) {
            relatedClasses.push([course.orgId + '_' + transPrefix + val]);
            allRelatedClsNbr.push([val]);
          })

          //console.log(relatedClasses[index])

          allRelatedTaken = relatedClasses[index].every(allClassesTaken);
          //console.log(allRelatedTaken)
          groupRelatedClasses.push(allRelatedTaken);

        })

        //console.log(groupRelatedClasses)

        /**
         * This function is to test to see if the class was taken by itself
         * and no other related class(es)
         */
        var totalFalseness = function(element){
          return element === false;
        }

        var something;
        if ( groupRelatedClasses.every(totalFalseness) ){
          // This means the course was taken by itself with
          console.log('Multiple Related courses taken by its lonesome ' )
          return calculateEquivalencies(uniqueId, [], currentProcessedCourse, finalObject, allRelatedTaken, transClsName, transCrseNbr, classKeys, []);

        }else {
          relatedClasses = [];
          allRelatedClsNbr = [];
          console.log('Multiple Related courses taken ' )

          groupRelatedClasses.forEach(function (data, index) {
            if (data === true) {
              // Both classes have been taken
              relatedClassArr[index].forEach(function (val) {
                relatedClasses.push(course.orgId + '_' + transPrefix + val);
                allRelatedClsNbr.push([val]);
              })
              //console.log(relatedClasses)
              something = calculateEquivalencies(uniqueId, relatedClasses, currentProcessedCourse, finalObject, allRelatedTaken, transClsName, transCrseNbr, classKeys, allRelatedClsNbr);
            }
          })
          if (something){
            return something;
          }
        }
      }else{
        // This mean we we have related class(es) with a single list of classes [ ['102', '103', '104'] ]
        // or no related classes.  This is the norm.
        console.log('Processing ' + transClsName)
        //console.log(relatedClassArr)

        // Check to see if all classes are taken

        if ( relatedClassArr.length > 0) {
          relatedClassArr.forEach(function (classArr) {
            classArr.forEach(function (val) {
              relatedClasses.push([course.orgId + '_' + transPrefix + val]);
              allRelatedClsNbr.push([val]);
            })
          })
          allRelatedTaken = relatedClasses.every(allClassesTaken);
          //console.log("Related Class arr length " + relatedClassArr.length)
          console.log('All related classes taken? ' + allRelatedTaken);
          return calculateEquivalencies(uniqueId, relatedClasses[0], currentProcessedCourse, finalObject, allRelatedTaken, transClsName, transCrseNbr, classKeys, allRelatedClsNbr[0]);

        }else{
          console.log('No related classes')
          return calculateEquivalencies(uniqueId, [], currentProcessedCourse, finalObject, allRelatedTaken, transClsName, transCrseNbr, classKeys, [])
        }
      }
    })
  )

}

// http://openmymind.net/Understanding-Javascript-Promises/
CourseEquivalency.prototype.getEquivalencyPromises = function () {

  return preProcessData(this.cartData)
    .then(processRuleData.bind(this))
    .then(processEquivalencies.bind(this));

}

module.exports.CourseEquivalency = CourseEquivalency;
