module.exports.CourseEquivalency = require('./courseEquivalency').CourseEquivalency
module.exports.CatalogData = require('./catalogData').CatalogData
module.exports.Organization = require('./organization').Organization
module.exports.Rule = require('./rule').Rule
