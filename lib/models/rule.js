'use strict';

var   mongoose = require('mongoose'),
  Schema = mongoose.Schema;
  // To get a logger instance, use the following (preferably you would not need a logger in a model)


/**
 * Example Schema
 * See: http://mongoosejs.com/docs/guide.html
 */

var RuleSchema = new Schema({
  TRNSFR_SRC_ID       : { type : String},
  COMP_SUBJECT_AREA   : { type : String },
  DATE_LOADED         : { type : Date},
  DESCR100            : { type : String},
  BEGIN_DT            : { type : Date},
  END_DT              : { type : Date},
  SCHOOL_SUBJECT      : { type : String},
  SCHOOL_CRSE_NBR     : { type : String},
  EXT_UNITS           : { type : Number},
  CRSE_ID             : { type : String},
  SUBJECT             : { type : String},
  CATALOG_NBR         : { type : String},
  UNT_TAKEN           : { type : Number},
  RQMNT_DESIGNTN      : { type : String}
});

RuleSchema.statics = {

  getCourseDetail : function(courseData, callback){
    //console.log(courseData);

    var queryDetails = {
      'TRNSFR_SRC_ID'   : courseData.orgId,
      'BEGIN_DT'        : {'$lte' : courseData.classTakenFinalDate} ,
      'END_DT'          : {'$gte' : courseData.classTakenFinalDate} ,
      'SCHOOL_SUBJECT'  : courseData.transPrefix,
      'SCHOOL_CRSE_NBR' : courseData.transCrseNbr
    }

    // convert options to query here
    return this.find(queryDetails, callback);
  },

  getCourse : function (query, callback){
    console.log(query)
    return this.find(query, callback).sort({ SCHOOL_SUBJECT: 1, SCHOOL_CRSE_NBR: 1 });
  }

};

// Virtuals
RuleSchema.virtual('value').get(function () { return this.SCHOOL_SUBJECT + ' ' + this.SCHOOL_CRSE_NBR ;});
RuleSchema.virtual('label').get(function () { return this.SCHOOL_SUBJECT + ' ' + this.SCHOOL_CRSE_NBR ;});


var TransRule = mongoose.model('TransRule', RuleSchema, 'rules');
exports.Model = exports.TransRule = TransRule;
exports.Schema = RuleSchema;