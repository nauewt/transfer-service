'use strict';

var mongoose = require('mongoose')
  , Schema = mongoose.Schema

// Schema
var AcadplanSchema = new Schema({
  INSTITUTION : String,
  ACAD_PLAN : String,
  N__CATALOG_YEAR : String,
  N__TEXT_EFFDT : String,
  EFFSEQ : Number,
  ACAD_PLAN_TYPE : String,
  TRNSCR_DESCR : String,
  DIPLOMA_DESCR : String,
  DESCR100 : String,
  FIRST_TERM_VALID  : String,
  N__FTV_DESCR  : String,
  N__LAST_TERM_VALID : String,
  N__LTV_DESCR : String,
  DEGREE : String,
  DESCRFORMAL : String,
  N__PLAN_LDESCR : String,
  N__PLAN_CAREER_OPP : String,
  N__PLAN_CAREERPATH : String,
  XLATLONGNAME  : String,
  N__PLAN_TYPE_GROUP : String,
  N__ACADUNIT_FDESCR : String,
  N__COLLEGE_FDESCR  : String,
  URL : String,
  N__CATALOG_URL : String,
  N__IMAGE_URL : String,
  N__CAMPUS_LIST : String,
  N__CAMP_CODE_LIST : String,
  N__FUTURE_FTV : String,
  N__CATEGORY_LIST : String,
  N__CAT_CODE_LIST : String,
  N__SBPLN_LIST  : String,
  N__AC_PLAN_DESCR: String,
  N__PLAN_MAJOR_GPA : String,
  N__MIN_UNITS_COMPL : Number,
  N__MAX_UNITS_COMPL : Number,
  N__PLAN_MAX_MATH : String,
  UNITS_MINIMUM : Number,
  UNITS_MAXIMUM : Number,
  SUBJECT : String,
  CATALOG_NBR : String,
  DESCRLONG: String,
  COURSE_TITLE_LONG : String,
  DESCR254A : String,
  ACAD_CAREER : String,
  N__PP_EXISTS : String,
  PUBLISH_FLAG : String,
  N__AREAOFINT_LIST : String,
  N__FACULTY_URL : String,
  N__PLAN_SLO : String
}, { autoIndex : (process.env.NODE_ENV || 'development').toLowerCase() !== 'production' })
// autoIndex should only be set in dev.

AcadplanSchema.index({ ACAD_PLAN : 1,  N__CAMPUS_LIST: 1, N__CATEGORY_LIST: 1})

AcadplanSchema.statics = {

  getCategory : function(query, callback){
    if ( query ){
      return this.find(query, callback);
    }else {
      return this.find({}, 'N__CATEGORY_LIST', callback);
    }
  },


  getCampus : function(query, callback){
    if ( query ){
      return this.find(query, callback);
    }else {
      return this.find({}, 'N__CAMPUS_LIST', callback);
    }
  },

  getPlans : function(query, callback){
    return this.find(query, callback);
  }

}

AcadplanSchema.virtual('campusCode').get(function () { return this.N__CAMP_CODE_LIST.split('|') })
AcadplanSchema.virtual('categoryCode').get(function () { return this.N__CAT_CODE_LIST.split('|') })
AcadplanSchema.virtual('acadDescr').get(function () {
  if ( this.DESCRFORMAL ){
    return this.DIPLOMA_DESCR + ', ' + this.DESCRFORMAL;
  }else{
    return this.DIPLOMA_DESCR + ', ' + this.N__PLAN_TYPE_GROUP
  }
})

var TransAcadplan = mongoose.model('TransAcadplan', AcadplanSchema, 'acadplans');

exports.Model = exports.TransAcadplan = TransAcadplan;
exports.Schema = AcadplanSchema;