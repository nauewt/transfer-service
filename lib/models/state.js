'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Schema
var StateSchema = new Schema({
  country: String,
  state: String,
  descr: String
});

// Virtuals
StateSchema.virtual('value').get(function () { return this.state; });
StateSchema.virtual('label').get(function () { return this.descr; });


var TransState = mongoose.model('TransState', StateSchema, 'states');
exports.Model = exports.TransState = TransState;
exports.Schema = StateSchema;