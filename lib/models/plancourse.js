
'use strict';

var mongoose = require('mongoose')
  , Schema = mongoose.Schema

// Schema
var PlancourseSchema = new Schema({
  ACAD_PLAN : String,
  DESCRFORMAL : String,
  DIPLOMA_DESCR : String,
  N__PLAN_TYPE_GROUP: String,
  CRSE_ID: String
}, { autoIndex: (process.env.NODE_ENV || 'development').toLowerCase() !== 'production' })
// autoIndex should only be set in dev.


PlancourseSchema.statics = {

  getPlanData : function(callback){
    return this.find({}, callback);
  }
}

PlancourseSchema.virtual('ACAD_PLAN_DESCR').get(function () {
  if ( this.DESCRFORMAL ){
    return this.DIPLOMA_DESCR + ', ' + this.DESCRFORMAL;
  }else{
    return this.DIPLOMA_DESCR + ', ' + this.N__PLAN_TYPE_GROUP
  }
})

var TransPlancourse = mongoose.model('TransPlancourse', PlancourseSchema, 'plancourses');

exports.Model = exports.TransPlancourse = TransPlancourse;
exports.Schema = PlancourseSchema;