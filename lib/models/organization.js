
'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Schema
var OrganizationSchema = new Schema({
  EXT_ORG_ID: Number,
  EFFDT: String,
  EFF_STATUS: String,
  DESCR: String,
  OTH_NAME_SORT_SRCH: String,
  EXT_ORG_TYPE: String,
  LS_SCHOOL_TYPE: String,
  CITY: String,
  STATE: String,
  COUNTRY: String,
  _meta : {}
}, { autoIndex: (process.env.NODE_ENV || 'development').toLowerCase() !== 'production' });
// autoIndex should only be set in dev.

OrganizationSchema.index({ DESCR: 1, CITY: 1, STATE: 1, COUNTRY: 1 });


OrganizationSchema.statics = {
  getOrganizationData : function(query, callback){
      return this.find(query, callback).sort({ '_meta.name': 1 });
  }
}


// Virtuals
OrganizationSchema.virtual('value').get(function () { return this.EXT_ORG_ID; });
OrganizationSchema.virtual('label').get(function () { return this._meta.name ;});


var TransOrganization = mongoose.model('TransOrganization', OrganizationSchema, 'organizations');
exports.Model = exports.TransOrganization = TransOrganization;
exports.Schema = OrganizationSchema;