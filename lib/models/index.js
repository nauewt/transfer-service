module.exports = {

  TransAcadplan: require('./acadplan').Model,
  TransOrganization: require('./organization').Model,
  TransPlancourse: require('./plancourse').Model,
  TransRule: require('./rule').Model,
  TransState: require('./state').Model
}